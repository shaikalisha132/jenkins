const mongoose=require("mongoose");

const PlasmaReceivalUserDetailsSchema=mongoose.Schema({
    phoneNumber: {
        type: String,
        require: true
    },
    
    username: {
        type: String,
        required: true
    },
    age:{
        type: Number,
        required:true
    },
    blood:{
        type: String,
        required:true
    },
    gender:{
        type: String,
        required:true
    },
    state:{
        type: String,
        required:true
    },
    district:{
        type: String,
        required:true
    },
    hospital:{
        type: String,
        required:true
    }
})



const PlasmaReceivalUserDetailsModel=mongoose.model("plasma-receivers",PlasmaReceivalUserDetailsSchema);


module.exports={PlasmaReceivalUserDetailsModel};