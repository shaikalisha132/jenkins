const mongoose = require("mongoose");

const PlasmaDonorDetails = mongoose.Schema({
    phoneNumber:{
        type: String,
        required: true
    },
    firstName: {
        type: String,
        required: true
    },
    lastName: {
        type: String,
        required: true
    },
    state: {
        type: String,
        required: true
    },
    district: {
        type: String,
        required: true
    },
    pincode:{
        type: String,
        required:true
    },
    hospitalName:{
        type: String,
        required:true
    }
})

const PlasmaDonorModel = mongoose.model('plasma-donors',PlasmaDonorDetails);

module.exports = { PlasmaDonorModel};