const mongoose = require("mongoose");

const VaccineDetailsSchema = mongoose.Schema({
    phoneNumber: {
        type: String,
        required: true
    },
    username: {
        type: String,
        required: true
    },
    document: {
        type: String,
        required: true
    },
    documentIdNumber: {
        type: String,
        required: true
    },
    state:{
        type: String,
        required:true
    },
    gender: {
        type: String,
        required: true
    },
    district:{
        type: String,
        required:true
    },
    postalCode:{
        type: String,
        required:true
    },
    yearOfBirth: {
        type: String,
        required:true
    },
    hospitalName:{
        type: String,
        required:true
    }
})

const VaccineRegistrationModel = mongoose.model('vaccination-registries',VaccineDetailsSchema);

module.exports={VaccineRegistrationModel};