import { AfterViewInit, Component, Input, OnInit } from '@angular/core';
import { DashboardServiceService } from 'src/app/services/dashboard-service.service';
import { Chart, registerables } from '../../../../node_modules/chart.js'

@Component({
  selector: 'app-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit, AfterViewInit {

  @Input() public parentData;
  chart: any;
  historicalData: any;
  historicalMonth: any;
  historicalYear: any;
  recovered = []
  confirmed = []
  deaths = []
  dayList = []
  months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
  years = ["2020", "2021"];

  constructor(private dashboardService: DashboardServiceService) { }

  ngOnInit(): void {

      this.dashboardService.getStateWiseHistoricalData()
        .then(response => {
          this.historicalData = response
          // this.getHistoricalData(this.historicalData);
        })
        .catch(err => console.log(err))

    Chart.register(...registerables);
  }

  ngAfterViewInit() {

    const data = {
      labels: this.dayList,
      datasets: [
        {
          label: 'Confirmed',
          data: this.confirmed,
          borderColor: 'red',
          // backgroundColor: 'green',
        },
        {
          label: 'Recovered',
          data: this.recovered,
          borderColor: 'blue',
          //backgroundColor: 'yelllow',
        },
        {
          label: "Deaths",
          data: this.deaths,
          borderColor: 'cyan',
          //backgroundColor: 'orange',
        }
      ]
    };

    const myChart = new Chart("myChart", {
      type: 'line',
      data: data,
      options: {
        layout: {
          padding: 20
        },
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
          },
          title: {
            display: true,
            text: 'Covid Situation in India'
          }
        }
      },
    });

    myChart.updateDataset();

    // if (!(this.historicalMonth && this.historicalYear)) {
    //   this.getHistoricalData();
    // }

  }

  getHistoricalData() {
    //console.log("Historical", this.historicalData)
    var k = 0;
    var c = 0;
    var data = [];
    var days = [];
    // var dayList = [];
    // var confirmed = [];
    // var recovered = [];
    // var deaths = [];
    var intmonth = this.months.indexOf(this.historicalMonth);
    var str = this.historicalYear + '-' + ('0' + (intmonth + 1)).slice(-2)
    var re = new RegExp(str)
    for (let i = 0; i < this.historicalData.data.length; i++) {
      if (this.historicalData.data[i].day.match(re)) {
        days[i] = this.historicalData.data[i].day
      }
      if (this.historicalData.data[i].day.match(re))
        for (let p = 0; p < this.historicalData.data[i].regional.length; p++) {
          if (this.historicalData.data[i].regional[p].loc === this.parentData) {
            data[k] = this.historicalData.data[i].regional[p];
            k++
          }
        }
    }
    for (let q = 0; q < days.length; q++) {
      if (days[q]) {
        this.dayList[c] = days[q];
        c++;
      }
    }
    for (let r = 0; r < data.length; r++) {
      this.confirmed[r] = data[r].confirmedCasesIndian
      this.recovered[r] = data[r].discharged
      this.deaths[r] = data[r].deaths
    }
    console.log("R", this.recovered);
    console.log("D", this.deaths);
    console.log("C", this.confirmed);
    console.log(this.dayList);
  }
}
