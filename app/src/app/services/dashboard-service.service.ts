import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DashboardServiceService {

  constructor(private http: HttpClient) { }

  requestStates() {
    return this.http.get<any>("../assets/data/districts.json")
  }

  getStates(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.requestStates().subscribe(
        response => resolve(response),
        err => reject(err)
      )
    })
  }

  requestStateWiseCovidData():Observable<any>{
    return this.http.get<any>("https://api.covid19india.org/data.json");
  }

  getStateWiseCovidData(): Promise<any>{
    return new Promise((resolve, reject) => {
      this.requestStateWiseCovidData().subscribe(
        response => { resolve(response); },
        err => { reject(err) }
      )
    })
  }

  requestDistrictWiseCovidData(){
    return this.http.get<any>("https://api.covid19india.org/state_district_wise.json")
  }

  getDistrictWiseCovidData(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.requestDistrictWiseCovidData().subscribe(
        response => { resolve(response) },
        err => { reject(err) }
      )
    })
  }

  requestStateWiseHistoricalData() {
    return this.http.get<any>("https://api.rootnet.in/covid19-in/stats/history")
  }

  getStateWiseHistoricalData(): Promise<any> {
    return new Promise((resolve, reject) => {
      this.requestStateWiseHistoricalData().subscribe(
        response => {resolve(response)},
        err => {reject(err)}
      )
    })
  }

  postSubscription(sub: PushSubscription){
    return this.http.post<any>("http://localhost:8016/subscribe", sub)
  }

}
