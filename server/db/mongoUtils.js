const mongoose = require("mongoose");
const URL = "mongodb+srv://ankit:Ankit123@cluster0.ivkfb.mongodb.net/department-login";


function mongooseVaccinationConnect() {
    mongoose.connect(URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(response => {
        console.log("Mongoose Vaccination Connected...")
    }).catch(err => console.log(err));
}

function mongooseHospitalConnect() {
    mongoose.createConnection(URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(response => {
        console.log("Mongoose Hospital Connected...")
    }).catch(err => console.log(err));
}

function mongooseAdminConnect() {
    mongoose.createConnection(URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(response => {
        console.log("Mongoose Admin Connected...")
    }).catch(err => console.log(err));

}
function mongoosePlasmaReceivalConnect() {
    mongoose.createConnection(URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true
    }).then(response => {
        console.log("Mongoose PlasmaReceival Connected...")
    }).catch(err => console.log(err));
}

function mongoosePlasmaDonors(){
    mongoose.createConnection(URL,{
        useNewUrlParser:true,
        useUnifiedTopology:true
    }).then(response=>{
        console.log("Mongoose Plasma Donors Connected...")
    }).catch(err=>console.log(err));
}

module.exports = { mongooseHospitalConnect, mongooseAdminConnect, mongooseVaccinationConnect, mongoosePlasmaReceivalConnect, mongoosePlasmaDonors };

