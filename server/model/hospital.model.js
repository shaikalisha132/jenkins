const mongoose=require("mongoose");

const hospitalSchema=mongoose.Schema({
    username: {
        type: String,
        required: true
    },
    password:{
        type: String,
        required:true
    },
    hospitalName:{
        type: String,
        required:true
    },
    state:{
        type: String,
        required:true
    },
    district:{
        type: String,
        required:true
    },
    postalCode:{
        type: String,
        required:true
    }
})

const AdminSchema = mongoose.Schema({
    username: {
        type: String,
        require: true
    },

    password: {
        type: String,
        require: true
    },

});


const HospitalModel=mongoose.model("hospital",hospitalSchema);
const AdminModel= mongoose.model('admin', AdminSchema);

module.exports={HospitalModel,AdminModel};
