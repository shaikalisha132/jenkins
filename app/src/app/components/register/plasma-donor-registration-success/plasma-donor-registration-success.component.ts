import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-plasma-donor-registration-success',
  templateUrl: './plasma-donor-registration-success.component.html',
  styleUrls: ['./plasma-donor-registration-success.component.css']
})
export class PlasmaDonorRegistrationSuccessComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
