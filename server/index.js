const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const mongoUtils = require("./db/mongoUtils");
const config = require('./config');
const client = require('twilio')(config.accountSID, config.authToken);
const webpush = require('web-push');
const {VaccineRegistrationModel}=require('./model/vaccineRegistriesDetail');
const { PlasmaReceivalUserDetailsModel } = require('./model/PlasmaReceival.model');
const { PlasmaDonorModel } = require('./model/plasma-donors.model');
const { HospitalModel } = require("./model/hospital.model");
const { AdminModel } = require("./model/hospital.model");
const app = express();
const jwt = require('jsonwebtoken');
//const { response } = require("express");

app.use(cors());
app.use(bodyParser.json());

mongoUtils.mongooseVaccinationConnect();
mongoUtils.mongooseAdminConnect();
mongoUtils.mongoosePlasmaDonors();
mongoUtils.mongooseHospitalConnect();
mongoUtils.mongoosePlasmaReceivalConnect();

app.get('/plasma-donors/:district',(req,res)=>{
    if (req.params) {
        const district = req.params.district;
        PlasmaDonorModel.find({ district })
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})

app.post("/plasma-receivers", (req, res) => {
    if (req.body) {
        const userDetailsList = new PlasmaReceivalUserDetailsModel(req.body);
        userDetailsList.save()
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err));
    }
})

app.post('/vaccination-registries', (req, res) => {
    if (req.body) {
        const vaccinationRegistries = new VaccineRegistrationModel(req.body);
        vaccinationRegistries.save()
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err));
    }
})

app.get("/vaccination-registries", (req, res) => {
    VaccineRegistrationModel.find((err, docs) => {
        if (err) console.log(err);
        return res.send(docs);
    })
})

app.get("/vaccination-registries-by-documentId/:documentIdNumber", (req, res) => {
    if(req.params) {
        const documentIdNumber = req.params.documentIdNumber;
        VaccineRegistrationModel.findOne({documentIdNumber})
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})

app.get("/plasma-receivers", (req, res) => {
    PlasmaReceivalUserDetailsModel.find((err, docs) => {
        if (err) console.log(err);
        return res.send(docs);
    })
})

app.get("/vaccination-registries-by-hospital/:hospitalName", (req, res) => {
    if(req.params) {
        const hospitalName = req.params.hospitalName;
        VaccineRegistrationModel.find({hospitalName})
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})

app.get("/admin", (req, res) => {
    AdminModel.find((err, docs) => {
        if (err) console.log(err);
        return res.send(docs);
    })
})

app.get('/getOTP', (req, res) => {
    client
        .verify
        .services(config.serviceID)
        .verifications
        .create({
            to: `+91${req.query.phonenumber}`,
            channel: 'sms'
        })
        .then((data) => {
            res.status(200).send(data);
        })
})

app.get('/verifyOTP', (req, res) => {
    client
        .verify
        .services(config.serviceID)
        .verificationChecks
        .create({
            to: `+91${req.query.phonenumber}`,
            code: req.query.code
        })
        .then((data) => {
            res.status(200).send(data);
        })
})

app.post('/subscribe', (req, res) => {
    let sub = req.body;
    res.set('Content-Type', 'application/json');
    webpush.setVapidDetails(
        'mailto:example@domain.com',
        config.publicKey,
        config.privateKey
    );

    let payload = JSON.stringify({
        "notification": {
            "title": "Covid Care",
            "body": "You have successfully subscribed to Covid Care Notifications",
            "icon": ""
        }
    })

    Promise.resolve(webpush.sendNotification(sub, payload))
        .then(() => {
            res.status(200).json({
                message: "Notification Sent"
            })
        })
        .catch(err => {
            console.log(err);
            res.sendStatus(500);
        })
})

app.post("/admin", (req, res) => {
    if (req.body) {
        const adminList = new AdminModel(req.body);
        adminList.save()
            .then(response => {
                let payload = { subject: response._id }
                let token = jwt.sign(payload, 'secretKey')
                res.status(200).send({ token });
            })
            .catch(err => console.log(err));
    }
})


app.get("/admin_by_username/:username", (req, res) => {
    if (req.params) {
        const username = req.params.username;
        AdminModel.findOne({ username })
            .then(response => {
                let payload = { subject: response._id };
                let token = jwt.sign(payload, 'secretKey');
                res.status(200).send({ token, response });
                // res.send(response);
            })
            .catch(err => console.log(err))
    }
})


app.get("/findby/:district", (req, res) => {

    if (req.params) {
        const district = req.params.district;
        HospitalModel.find({ 'district' : district })
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})

app.get("/hospital", (req, res) => {
    HospitalModel.find((err, docs) => {
        if (err) console.log(err);
        return res.send(docs);
    })
})

app.get("/plasma-donors", (req, res) => {
    PlasmaDonorModel.find((err, docs) => {
        if (err) console.log(err);
        return res.send(docs);
    })
})

app.post("/hospital", (req, res) => {
    if (req.body) {
        const hsptlList = new HospitalModel(req.body);
        hsptlList.save()
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err));
    }
})

app.post('/plasma-donors', (req, res) => {
    if (req.body) {
        const plasmaDonors = new PlasmaDonorModel(req.body);
        plasmaDonors.save()
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err));
    }
})

app.get("/plasma-donors-by-hospitalName/:hospitalName", (req, res) => {
    if(req.params) {
        const hospitalName = req.params.hospitalName;
        PlasmaDonorModel.find({hospitalName})
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})

/* app.post("/plasma-donors/hospitalName", (req, res) => {
    if(req.body) {
        const hospitalName = req.body.hospitalName;
        PlasmaDonorModel.find({hospitalName:req.body.hospitalName})
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
}) */


/* app.get("/hospital/:username", (req, res) => {
    if (req.params) {
        const username = req.params.username;
        HospitalModel.findOne({ username })
            .then(response =>
            {
                let payload = { subject: response._id };
                let token = jwt.sign(payload, 'secretKey');
                res.status(200).send({ token, response });
                // return res.send(response);
            })
            .catch(err => console.log(err))
    }
})
 */
app.get("/hospital/:username", (req, res) => {
    if (req.params) {
        const username = req.params.username;
        HospitalModel.findOne({ username })
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})
app.get("/hospitalSingleData/:hospitalName", (req, res) => {
    if (req.params) {
        const hospitalName = req.params.hospitalName;
        HospitalModel.find({ hospitalName })
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})

app.delete("/hospital/:username", (req, res) => {
    if (req.params) {
        const username = req.params.username;
        HospitalModel.deleteOne({ username })
            .then(response => {
                return res.send(response);
            })
            .catch(err => console.log(err))
    }
})




app.listen(8016, () => {
    console.log("Server started at PORT 8016");
})



