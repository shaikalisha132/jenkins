import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RegisterService } from '../../../services/register.service'

@Component({
  selector: 'app-register-plasma-donation',
  templateUrl: './register-plasma-donation.component.html',
  styleUrls: ['./register-plasma-donation.component.css']
})
export class RegisterPlasmaDonationComponent implements OnInit {

  phoneNumber : any = "";
  otp : any
  data : any
  value = ''
  data2 : any
  status: boolean = false
  r: any = ''
  constructor(private registerService : RegisterService, private router: Router ) { }

  ngOnInit(): void {

    
  }

  getOTP(phoneNumber:string){

    this.registerService.requestOtp(phoneNumber)
      .subscribe(response => console.log(response))

  }

  veriifyOTP(){

    if(this.phoneNumber && this.value){
      this.registerService.otpVerification(this.phoneNumber,this.value)
          .then(res => {
            this.r = res.status
            if(this.r){
              if(this.r === 'approved'){
                this.router.navigate(['/plasma-donor-details'])
                localStorage.setItem('phoneNumber', this.phoneNumber)
              }else{
                this.value = '';
                alert("Enter Correct OTP")
              }
            }
          })
          .catch(err => console.log(err))
      }
  
  }//veriifyOTP 

  


}